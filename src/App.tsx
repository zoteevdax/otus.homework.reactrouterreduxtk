import { useEffect, useState } from "react";
import { Routes, Route, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import HomePage from "./pages/HomePage";
import NotFoundPage from "./pages/NotFoundPage";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import UserPublicationsPage from "./pages/UserPublicationsPage";
import NewPostPage from "./pages/NewPostPage";

import Loader from "./components/Loader";
import AppNavbar from "./components/AppNavbar";

import { getPosts } from "./services/postsService";
import { getUsers } from "./services/usersService";

import { initPosts } from "./reducers/postsSlice";
import { initUsers } from "./reducers/usersSlice";
import { logoutUser } from "./reducers/authSlice";

import { type RootState } from "./store";

import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { isLoggedIn, user } = useSelector((state: RootState) => state.auth);

  const [loading, setLoading] = useState(true);

  async function initFakeData() {
    const users = await getUsers();
    const posts = await getPosts();

    dispatch(initPosts(posts));
    dispatch(initUsers(users));

    setLoading(false);
  }

  function handleLogout() {
    dispatch(logoutUser());
    navigate("/login");
  }

  useEffect(() => {
    void initFakeData();
  }, []);

  if (loading) {
    return <Loader />;
  }

  return (
    <>
      <AppNavbar isLoggedIn={isLoggedIn} user={user} logout={handleLogout} />

      <main className="container-md">
        <Routes>
          <Route index path="/" element={<HomePage />} />
          <Route path="myPublications">
            <Route index element={<UserPublicationsPage isLoggedIn={isLoggedIn} />} />
            <Route path="new" element={<NewPostPage isLoggedIn={isLoggedIn} />} />
          </Route>
          <Route path="login" element={<LoginPage />} />
          <Route path="register" element={<RegisterPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </main>
    </>
  );
}

export default App;
