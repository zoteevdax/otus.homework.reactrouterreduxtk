import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import { useDispatch, useSelector } from "react-redux";
import { type RootState } from "../store";
import { loginUser } from "../reducers/authSlice";

export default function LoginPage() {
  const navigate = useNavigate();
  const dispath = useDispatch();

  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [validated, setValidated] = useState(false);

  const [error, setError] = useState("");

  const auth = useSelector((state: RootState) => state.auth);
  const { users } = useSelector((state: RootState) => state.users);

  if (auth.isLoggedIn) {
    navigate("/");
  }

  function getUser(login: string, password: string) {
    return users.find((u) => u.login === login && u.password === password);
  }

  function handleSubmit(event: React.SyntheticEvent<HTMLFormElement>) {
    event.preventDefault();
    event.stopPropagation();
    setValidated(false);

    setError("");

    const form = event.currentTarget;
    if (!form.checkValidity()) {
      setValidated(true);
      return;
    }

    const user = getUser(login, password);
    if (user == null) {
      setError("Incorrect login or password");
      return;
    }

    dispath(loginUser(user));
    navigate("/");
  }

  return (
    <div className="d-flex flex-column w-100 justify-content-center align-items-center">
      <h3>Login</h3>
      <Form className="w-50 p-5 border" noValidate validated={validated} onSubmit={handleSubmit}>
        <Form.Group className="mb-3" controlId="formBasicUserLogin">
          <Form.Label>Login</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="Enter login"
            value={login}
            onChange={(event) => {
              setLogin(event.currentTarget.value);
            }}
          />
          <Form.Control.Feedback type="invalid">Please provide login</Form.Control.Feedback>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            required
            type="password"
            placeholder="Password"
            value={password}
            onChange={(event) => {
              setPassword(event.currentTarget.value);
            }}
          />
          <Form.Control.Feedback type="invalid">Please provide password</Form.Control.Feedback>
        </Form.Group>

        <Form.Text className="text-danger">{error}</Form.Text>

        <div className="d-flex justify-content-center">
          <Button variant="primary" type="submit" size="lg">
            Login
          </Button>
        </div>
      </Form>
    </div>
  );
}
