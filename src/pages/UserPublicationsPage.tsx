import { Link } from "react-router-dom";
import { type RootState } from "../store";
import { useDispatch, useSelector } from "react-redux";
import UserPost from "../components/user/UserPost";
import { Stack } from "react-bootstrap";
import { removePost } from "../reducers/postsSlice";
import { withProtection } from "../components/HOC/withProtection";

const UserPublicationsPage = () => {
  const dispatch = useDispatch();

  const { user, isLoggedIn } = useSelector((state: RootState) => state.auth);
  const userPosts = useSelector((state: RootState) => state.posts.list.filter((p) => p.userId === user?.id));

  const userName = user != null ? `${user.firstName} ${user.lastName}` : "";

  function handlePostRemove(id: number) {
    dispatch(removePost(id));
  }

  return (
    <Stack gap={3}>
      <div className="d-flex justify-content-center align-items-center gap-2">
        <h3>My posts</h3>
        <Link to="new">Добавить</Link>
      </div>

      {userPosts.map((p) => (
        <UserPost
          key={p.id}
          title={p.title}
          author={userName}
          text={p.body}
          allowEdit={isLoggedIn}
          onDelete={() => {
            handlePostRemove(p.id);
          }}
        />
      ))}
    </Stack>
  );
};

export default withProtection(UserPublicationsPage);
