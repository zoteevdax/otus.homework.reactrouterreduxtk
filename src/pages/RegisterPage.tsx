import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Col, Row } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import { useDispatch, useSelector } from "react-redux";
import { registerUser } from "../reducers/usersSlice";
import { type RootState } from "../store";
import Loader from "../components/Loader";

export default function RegisterPage() {
  const dispath = useDispatch();
  const navigate = useNavigate();

  const { isLoggedIn } = useSelector((state: RootState) => state.auth);
  const { users } = useSelector((state: RootState) => state.users);

  if (isLoggedIn) {
    navigate("/login");
  }

  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [loginError, setloginError] = useState("");

  const [validated, setValidated] = useState(false);
  const [loading, setLoading] = useState(false);

  function userExist(login: string) {
    return users.find((u) => u.login === login) !== undefined;
  }

  async function handleSubmit(event: React.SyntheticEvent<HTMLFormElement>) {
    event.preventDefault();
    event.stopPropagation();
    setValidated(false);

    setValidated(false);
    setloginError("");

    const form = event.currentTarget;
    if (!form.checkValidity()) {
      setValidated(true);
      return;
    }

    const userProfile = {
      login,
      password,
      firstName,
      lastName,
    };

    if (userExist(login)) {
      setloginError("User with this login is already registered");
      return;
    }

    setLoading(true);

    const res = await fetch("https://dummyjson.com/users/add", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(userProfile),
    });

    if (res.ok) {
      dispath(registerUser({ id: 0, ...userProfile }));
      navigate("/login");
    }
  }

  if (loading) {
    return <Loader />;
  }

  return (
    <div className="d-flex flex-column w-100 justify-content-center align-items-center">
      <h3>Register</h3>
      <Form className="w-50 p-5 border" noValidate validated={validated} onSubmit={handleSubmit}>
        <Form.Group className="mb-3" controlId="formBasicUserLogin">
          <Form.Label>Login</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="Enter login"
            value={login}
            onChange={(event) => {
              setLogin(event.currentTarget.value);
            }}
          />
          <Form.Control.Feedback type="invalid">"Please provide login"</Form.Control.Feedback>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            required
            type="password"
            placeholder="Password"
            value={password}
            onChange={(event) => {
              setPassword(event.currentTarget.value);
            }}
          />
          <Form.Control.Feedback type="invalid">Please provide password</Form.Control.Feedback>
        </Form.Group>

        <Row>
          <Col>
            <Form.Group className="mb-3" controlId="formBasicFirstName">
              <Form.Label>FirstName</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="FirstName"
                value={firstName}
                onChange={(event) => {
                  setFirstName(event.currentTarget.value);
                }}
              />
              <Form.Control.Feedback type="invalid">Please provide firstName</Form.Control.Feedback>
            </Form.Group>
          </Col>

          <Col>
            <Form.Group className="mb-3" controlId="formBasicLastName">
              <Form.Label>LastName</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="LastName"
                value={lastName}
                onChange={(event) => {
                  setLastName(event.currentTarget.value);
                }}
              />
              <Form.Control.Feedback type="invalid">Please provide lastName</Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>

        <Form.Text className="text-danger">{loginError}</Form.Text>

        <div className="d-flex justify-content-center mt-2">
          <Button variant="primary" type="submit" size="lg">
            Register
          </Button>
        </div>
      </Form>
    </div>
  );
}
