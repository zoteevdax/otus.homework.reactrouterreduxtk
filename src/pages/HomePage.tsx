import React from "react";
import { Stack } from "react-bootstrap";
import { useSelector } from "react-redux";
import { type RootState } from "../store";
import UserPost from "../components/user/UserPost";

export default function HomePage() {
  const posts = useSelector((state: RootState) => state.posts.list);
  const users = useSelector((state: RootState) => state.users.users);

  function getAuthorName(id: number) {
    const user = users.find((u) => u.id === id);
    if (user != null) {
      return `${user.firstName} ${user.lastName}`;
    }

    return "unknown";
  }

  return (
    <Stack gap={3}>
      <h3 className="m-auto">Last users posts</h3>
      {posts.map((p) => (
        <UserPost key={p.id} title={p.title} author={getAuthorName(p.userId)} text={p.body} allowEdit={false} />
      ))}
    </Stack>
  );
}
