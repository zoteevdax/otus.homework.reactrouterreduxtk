export default function NotFoundPage() {
  return (
    <div className="w-100 h-100 d-flex flex-column justify-content-center align-items-center">
      <h1>404</h1>
      <p>Page not found</p>
    </div>
  );
}
