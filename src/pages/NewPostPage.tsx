import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { type RootState } from "../store";
import { Button } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import { useState } from "react";
import { addPost } from "../reducers/postsSlice";
import { withProtection } from "../components/HOC/withProtection";

const NewPostPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { user } = useSelector((state: RootState) => state.auth);

  const [title, setTitle] = useState("");
  const [text, setText] = useState("");
  const [validated, setValidated] = useState(false);

  function handleSubmit(event: React.SyntheticEvent<HTMLFormElement>) {
    event.preventDefault();
    event.stopPropagation();

    if (user == null) {
      return;
    }

    const form = event.currentTarget;
    if (!form.checkValidity()) {
      return;
    }

    setValidated(true);

    dispatch(
      addPost({
        title,
        body: text,
        userId: user.id,
      }),
    );
    navigate("/myPublications");
  }

  return (
    <div className="d-flex flex-column w-100 justify-content-center align-items-center">
      <h3>New post</h3>
      <Form className="w-50 p-5 border" noValidate validated={validated} onSubmit={handleSubmit}>
        <Form.Group className="mb-3" controlId="formBasicTitle">
          <Form.Label>Title</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="Enter title"
            value={title}
            onChange={(event) => {
              setTitle(event.currentTarget.value);
            }}
          />
          <Form.Control.Feedback type="invalid">Please provide title</Form.Control.Feedback>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicText">
          <Form.Label>Text</Form.Label>
          <Form.Control
            required
            type="text"
            as="textarea"
            rows={3}
            placeholder="post body"
            value={text}
            onChange={(event) => {
              setText(event.currentTarget.value);
            }}
          />
          <Form.Control.Feedback type="invalid">Please provide post text</Form.Control.Feedback>
        </Form.Group>

        <div className="d-flex justify-content-center">
          <Button variant="primary" type="submit" size="lg">
            Save
          </Button>
        </div>
      </Form>
    </div>
  );
};

export default withProtection(NewPostPage);
