import { configureStore } from "@reduxjs/toolkit";
import usersReducer from "./reducers/usersSlice";
import postsReducer from "./reducers/postsSlice";
import authReducer from "./reducers/authSlice";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    users: usersReducer,
    posts: postsReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
