import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { type UserProfile } from "../types/userTypes";

interface UsersState {
  counter: number;
  users: UserProfile[];
}

const initialState: UsersState = {
  counter: 0,
  users: [],
};

export const usersSlice = createSlice({
  name: "users",
  initialState,
  reducers: {
    initUsers: (state, action: PayloadAction<UserProfile[]>) => {
      const count = action.payload.length;
      state.counter = count + 100;

      state.users = action.payload;
    },
    registerUser: (state, action: PayloadAction<UserProfile>) => {
      const { login } = action.payload;
      if (state.users.find((u) => u.login === login) == null) {
        state.users.push({ ...action.payload, id: state.counter + 1 });
        state.counter += 1;
      }
    },
  },
});

export const { initUsers, registerUser } = usersSlice.actions;

export default usersSlice.reducer;
