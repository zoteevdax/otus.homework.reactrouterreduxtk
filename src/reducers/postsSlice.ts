import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
interface NewPostProps {
  title: string;
  body: string;
  userId: number;
}

interface PostProps extends NewPostProps {
  id: number;
}
interface PostsState {
  counter: number;
  list: PostProps[];
}

const initialState: PostsState = {
  counter: 0,
  list: [],
};

export const postsSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    initPosts: (state, action: PayloadAction<PostProps[]>) => {
      state.counter = action.payload.length + 100;
      state.list = action.payload;
    },
    addPost: (state, action: PayloadAction<NewPostProps>) => {
      state.list.splice(0, 0, { ...action.payload, id: state.counter });
      state.counter += 1;
    },
    removePost: (state, action: PayloadAction<number>) => {
      state.list = state.list.filter((p) => p.id !== action.payload);
    },
  },
});

export const { initPosts, addPost, removePost } = postsSlice.actions;

export default postsSlice.reducer;
