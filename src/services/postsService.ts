export async function getPosts() {
  const res = await fetch("https://dummyjson.com/posts");
  if (res.ok) {
    return (await res.json()).posts;
  }
}
