interface FetchData {
  id: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
}

export async function getUsers() {
  const res = await fetch("https://dummyjson.com/users");
  if (res.ok) {
    return (await res.json()).users.map((u: FetchData) => ({
      id: u.id,
      login: u.username,
      password: u.password,
      firstName: u.firstName,
      lastName: u.lastName,
    }));
  }
}
