import { Card, Button } from "react-bootstrap";

interface UserPostProps {
  title: string;
  author: string;
  text: string;
  allowEdit: boolean;
  onDelete?: () => void;
}

export default function UserPost({ title, author, text, allowEdit, onDelete }: UserPostProps) {
  return (
    <Card className="w-100">
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">Author: {author}</Card.Subtitle>
        <Card.Text>{text}</Card.Text>

        {allowEdit && <Button onClick={onDelete}>Delete</Button>}
      </Card.Body>
    </Card>
  );
}
