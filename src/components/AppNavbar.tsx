import { Navbar, Container, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import { type UserProfile } from "../types/userTypes";

interface AppNavbarProps {
  isLoggedIn: boolean;
  user?: UserProfile | null;
  logout: () => void;
}

export default function AppNavbar({ user, isLoggedIn, logout }: AppNavbarProps) {
  const userName = user != null ? `${user.firstName} ${user.lastName}` : "";

  return (
    <Navbar expand="sm" bg="primary" data-bs-theme="dark" fixed="top">
      <Container>
        <Navbar.Brand as={Link} to="/">
          UsersPosts
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/">
              Home
            </Nav.Link>
            {isLoggedIn && (
              <Nav.Link as={Link} to="/myPublications">
                MyPublications
              </Nav.Link>
            )}
          </Nav>

          {isLoggedIn ? (
            <Nav>
              <Navbar.Text>
                Signed in as: <span className="text-white">{userName}</span>
              </Navbar.Text>
              <Nav.Link onClick={logout}>Logout</Nav.Link>
            </Nav>
          ) : (
            <Nav>
              <Nav.Link as={Link} to="/login">
                Login
              </Nav.Link>
              <Nav.Link as={Link} to="/register">
                Register
              </Nav.Link>
            </Nav>
          )}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
