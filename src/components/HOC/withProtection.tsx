import { Stack } from "react-bootstrap";
import { NavLink } from "react-router-dom";

export interface AuthProps {
  isLoggedIn: boolean;
}

export const withProtection = <P extends AuthProps>(Component: React.ComponentType<P>) => {
  const componentWithAuth = ({ isLoggedIn, ...props }: P) => {
    if (isLoggedIn) {
      return <Component {...(props as P)} />;
    }

    return (
      <Stack className="justify-content-center align-items-center">
        <p>You do not have access to this page</p>
        <NavLink className="text-primary" to="/login">
          login
        </NavLink>
      </Stack>
    );
  };

  componentWithAuth.displayName = `withProtection(${Component.name ?? "Page"})`;
  return componentWithAuth;
};
