import { Spinner } from "react-bootstrap";

export default function Loader() {
  return (
    <div className="w-100 h-100 d-flex justify-content-center align-items-center">
      <Spinner animation="border" variant="primary" />
    </div>
  );
}
