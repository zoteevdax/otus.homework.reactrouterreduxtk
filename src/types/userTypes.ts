export interface UserProfile {
  id: number;
  login: string;
  password: string;
  firstName: string;
  lastName: string;
}
